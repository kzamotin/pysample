# Changelog

### 18.12.2020

- Added pipeline status
- Coverage report
- Allure report link

### 08.12.2020

- Travic-ci integration

### 04.12.2020

- Added an example of working with pytest

- Smoke-test command line

```
pytest -v -m smoke --alluredir=alluredir
```

- All test's run command

```
pytest -v --alluredir=alluredir
```

- To view results

```
allure serve ./alluredir
```

### Sample test suite

![pipeline status](https://gitlab.com/kzamotin/pysample/badges/main/pipeline.svg)

![](https://travis-ci.org/kzamotin/pysample.svg?branch=main&status=passed)

![coverage report](https://gitlab.com/kzamotin/pysample/badges/main/coverage.svg)


### Current pipeline report

[Artifact](https://kzamotin.gitlab.io/-/pysample/-/jobs/921265251/artifacts/public/index.html)
